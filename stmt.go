package sqlx

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
)

// Stmt is a prepared statement.
type Stmt struct {
	err   error
	cmd   string
	tx    *Tx
	rstmt *sql.Stmt
	pargs []any
	nargs map[string]any
}

// Exec executes a query without returning any rows.
func (s *Stmt) Exec(args ...any) (Result, error) {
	return s.ExecContext(context.Background(), args...)
}

// ExecContext executes a query without returning any rows.
func (s *Stmt) ExecContext(ctx context.Context, args ...any) (Result, error) {
	if s.tx.readonly {
		return nil, ErrReadOnly
	}

	rstmt, err := s.getRawStmt(ctx)
	if err != nil {
		return nil, err
	}
	return rstmt.ExecContext(ctx, s.buildArgs(args)...)
}

// Query executes a query.
func (s *Stmt) Query(result any, args ...any) error {
	return s.QueryContext(context.Background(), result, args...)
}

// QueryContext executes a query.
func (s *Stmt) QueryContext(ctx context.Context, result any, args ...any) (err error) {
	if result == nil {
		return errors.New("sqlx: result is nil")
	}

	v := reflect.ValueOf(result)
	t := v.Type()
	if t.Kind() != reflect.Pointer {
		return fmt.Errorf("sqlx: result is not a pointer: %s", t)
	}
	t, v = t.Elem(), v.Elem()

	iter := s.QueryRowsContext(ctx, args...)
	if t.Kind() == reflect.Slice {
		return scanSlice(iter, t, v)
	}
	return scanSingle(iter, t, v)
}

// QueryRows executes a query that returns rows.
func (s *Stmt) QueryRows(args ...any) func(func(*Row, error) bool) {
	return s.QueryRowsContext(context.Background(), args...)
}

// QueryRowsContext executes a query that returns rows.
func (s *Stmt) QueryRowsContext(ctx context.Context, args ...any) func(func(*Row, error) bool) {
	return func(yield func(*Row, error) bool) {
		rstmt, err := s.getRawStmt(ctx)
		if err != nil {
			yield(nil, err)
			return
		}
		rows, err := rstmt.QueryContext(ctx, s.buildArgs(args)...)
		if err != nil {
			yield(nil, err)
			return
		}
		defer rows.Close()

		columns, err := rows.Columns()
		if err != nil {
			yield(nil, err)
			return
		}
		fields := make([]any, len(columns))

		for rows.Next() {
			if !yield(&Row{rows: rows, columns: columns, fields: fields}, nil) {
				return
			}
		}

		if err := rows.Err(); err != nil {
			yield(nil, err)
		}
	}
}

func (s *Stmt) getRawStmt(ctx context.Context) (*sql.Stmt, error) {
	if s.rstmt == nil && s.err == nil {
		rtx, err := s.tx.getRawTx(ctx)
		if err != nil {
			s.err = err
			return s.rstmt, s.err
		}
		s.rstmt, s.err = rtx.PrepareContext(ctx, s.cmd)
	}
	return s.rstmt, s.err
}

func (s *Stmt) reset() {
	if len(s.pargs) > 0 {
		s.pargs = s.pargs[:0]
		for param := range s.nargs {
			s.nargs[param] = nil
		}
	}
}

func (s *Stmt) buildArgs(args []any) []any {
	s.reset()

	for _, arg := range args {
		s.buildArg(arg)
	}

	for name, value := range s.nargs {
		s.pargs = append(s.pargs, sql.Named(name, value))
	}
	return s.pargs
}

func (s *Stmt) buildArg(arg any) {
	if namedArg, ok := arg.(sql.NamedArg); ok {
		s.buildNamedArg(namedArg)
		return
	}

	v := reflect.ValueOf(arg)
	t := v.Type()
	if t.Kind() == reflect.Struct {
		s.buildStructArg(t, v)
		return
	}
	if t.Kind() == reflect.Pointer {
		if t := t.Elem(); t.Kind() == reflect.Struct {
			s.buildStructArg(t, v.Elem())
			return
		}
	}

	s.pargs = append(s.pargs, arg)
}

func (s *Stmt) buildNamedArg(arg sql.NamedArg) {
	if _, ok := s.nargs[arg.Name]; ok {
		s.nargs[arg.Name] = arg.Value
	}
}

func (s *Stmt) buildStructArg(t reflect.Type, v reflect.Value) {
	for name := range s.nargs {
		if fv, err := getFieldValue(t, v, name, false); err == nil {
			s.nargs[name] = fv.Interface()
		}
	}
}
