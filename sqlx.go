// Package sqlx provides extra features for go's standard database/sql library.
package sqlx

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"sync"
)

// A Result summarizes an executed SQL command.
type Result = sql.Result

// IsolationLevel is the transaction isolation level.
type IsolationLevel = sql.IsolationLevel

// Isolation levels.
const (
	LevelDefault         = sql.LevelDefault
	LevelReadUncommitted = sql.LevelReadUncommitted
	LevelReadCommitted   = sql.LevelReadCommitted
	LevelWriteCommitted  = sql.LevelWriteCommitted
	LevelRepeatableRead  = sql.LevelRepeatableRead
	LevelSnapshot        = sql.LevelSnapshot
	LevelSerializable    = sql.LevelSerializable
	LevelLinearizable    = sql.LevelLinearizable
)

// Errors.
var (
	ErrConnDone  = sql.ErrConnDone
	ErrTxDone    = sql.ErrTxDone
	ErrEmpty     = sql.ErrNoRows
	ErrNotUnique = errors.New("sqlx: result is not unique")
	ErrReadOnly  = errors.New("sqlx: transaction is readonly")
)

var (
	structIndexesMutex sync.RWMutex
	structIndexes      = make(map[reflect.Type]map[string]int)
	regexpParam        = regexp.MustCompile(`[:@$]\w+`)
)

// Query executes a query.
func Query[T any](stmt *Stmt, args ...any) func(func(T, error) bool) {
	return QueryContext[T](context.Background(), stmt, args...)
}

// QueryContext executes a query.
func QueryContext[T any](ctx context.Context, stmt *Stmt, args ...any) func(func(T, error) bool) {
	return func(yield func(T, error) bool) {
		iter := stmt.QueryRowsContext(ctx, args...)
		iter(func(row *Row, err error) bool {
			pointer := new(T) // Ensure addressable.
			if err != nil {
				yield(*pointer, err)
				return false
			}

			v := reflect.ValueOf(pointer).Elem()
			t := v.Type()
			if t.Kind() == reflect.Pointer {
				t = t.Elem()
				v.Set(reflect.New(t))
				v = v.Elem()
			}

			err = row.scan(t, v)
			return yield(*pointer, err)
		})
	}
}

// Single returns one and only one value.
func Single[T any](stmt *Stmt, args ...any) (value T, err error) {
	return SingleContext[T](context.Background(), stmt, args...)
}

// SingleContext returns one and only one value.
func SingleContext[T any](ctx context.Context, stmt *Stmt, args ...any) (value T, err error) {
	isUnique := true
	iter := QueryContext[T](ctx, stmt, args...)
	iter(func(v T, e error) bool {
		if e != nil {
			err = e
			return false
		}
		if !isUnique {
			err = ErrNotUnique
			return false
		}
		isUnique = false
		value = v
		return true
	})
	return
}

func scanSlice(iter func(func(*Row, error) bool), t reflect.Type, v reflect.Value) (err error) {
	et := t.Elem()
	isp := false
	if et.Kind() == reflect.Pointer {
		et = et.Elem()
		isp = true
	}
	if et.Kind() != reflect.Struct {
		return fmt.Errorf("sqlx: unsupported type: %s", et)
	}

	xs := v
	iter(func(r *Row, e error) bool {
		if e != nil {
			err = e
			return false
		}

		ep := reflect.New(et)
		ev := ep.Elem()
		if err = r.scan(et, ev); err != nil {
			return false
		}

		if isp {
			xs = reflect.Append(xs, ep)
		} else {
			xs = reflect.Append(xs, ev)
		}
		return true
	})

	if err == nil {
		v.Set(xs)
	}
	return
}

func scanSingle(iter func(func(*Row, error) bool), t reflect.Type, v reflect.Value) (err error) {
	isUnique := true
	iter(func(r *Row, e error) bool {
		if e != nil {
			err = e
			return false
		}
		if !isUnique {
			err = ErrNotUnique
			return false
		}
		isUnique = false
		err = r.scan(t, v)
		return true
	})
	return
}

func getFieldValue(t reflect.Type, v reflect.Value, name string, create bool) (reflect.Value, error) {
	indexes := getStructIndexes(t)
	i := strings.IndexByte(name, '_')
	key := name
	if i >= 0 {
		key = name[:i]
	}
	index, ok := indexes[key]
	if !ok {
		return reflect.Value{}, fmt.Errorf("sqlx: field not found: %v[%v]", t, name)
	}

	fv := v.FieldByIndex([]int{index})
	if i < 0 {
		return fv, nil
	}

	ft := fv.Type()
	if ft.Kind() == reflect.Pointer {
		ft = ft.Elem()
		if fv.IsNil() {
			if !create {
				return reflect.Value{}, fmt.Errorf("sqlx: field is nil: %v[%v]", t, key)
			}
			fv.Set(reflect.New(ft))
		}
		fv = fv.Elem()
	}

	if ft.Kind() != reflect.Struct {
		return reflect.Value{}, fmt.Errorf("sqlx: unsupported type for %v: %v", name, ft)
	}
	return getFieldValue(ft, fv, name[i+1:], create)
}

func getStructIndexes(t reflect.Type) map[string]int {
	structIndexesMutex.RLock()
	indexes, ok := structIndexes[t]
	structIndexesMutex.RUnlock()
	if ok {
		return indexes
	}

	structIndexesMutex.Lock()
	defer structIndexesMutex.Unlock()

	if indexes, ok = structIndexes[t]; ok {
		return indexes
	}

	n := t.NumField()
	indexes = make(map[string]int, n)
	structIndexes[t] = indexes
	for i := 0; i < n; i++ {
		f := t.Field(i)
		if f.IsExported() {
			key := f.Name
			if tag, ok := f.Tag.Lookup("db"); ok {
				key = tag
			}
			indexes[key] = i
		}
	}
	return indexes
}
