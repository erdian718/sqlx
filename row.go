package sqlx

import (
	"database/sql"
	"reflect"
)

// Rows is a single result of a query.
type Row struct {
	rows    *sql.Rows
	columns []string
	fields  []any
}

// Scan copies the columns from the matched row into the values pointed at by dest.
func (r *Row) Scan(dest ...any) error {
	return r.rows.Scan(dest...)
}

func (r *Row) scan(t reflect.Type, v reflect.Value) error {
	for i, column := range r.columns {
		fv, err := getFieldValue(t, v, column, true)
		if err != nil {
			return err
		}
		r.fields[i] = fv.Addr().Interface()
	}
	return r.Scan(r.fields...)
}
