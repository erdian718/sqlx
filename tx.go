package sqlx

import (
	"context"
	"database/sql"
)

// Tx is an in-progress database transaction.
type Tx struct {
	Isolation IsolationLevel

	err      error
	readonly bool
	rdb      *sql.DB
	rtx      *sql.Tx
}

// Stmt creates a prepared statement for later queries or executions.
func (tx *Tx) Stmt(cmd string) *Stmt {
	names := regexpParam.FindAllString(cmd, -1)
	nargs := make(map[string]any, len(names))
	for _, name := range names {
		nargs[name[1:]] = nil
	}
	return &Stmt{
		cmd:   cmd,
		tx:    tx,
		nargs: nargs,
	}
}

// Rollback aborts the transaction.
func (tx *Tx) Rollback() error {
	if tx.rtx == nil {
		return nil
	}
	if err := tx.rtx.Rollback(); err != nil {
		return err
	}
	tx.rtx = nil
	return nil
}

// Commit commits the transaction.
func (tx *Tx) Commit() error {
	if tx.rtx == nil {
		return nil
	}
	if err := tx.rtx.Commit(); err != nil {
		return err
	}
	tx.rtx = nil
	return nil
}

func (tx *Tx) getRawTx(ctx context.Context) (*sql.Tx, error) {
	if tx.rtx == nil && tx.err == nil {
		tx.rtx, tx.err = tx.rdb.BeginTx(ctx, &sql.TxOptions{Isolation: tx.Isolation, ReadOnly: tx.readonly})
	}
	return tx.rtx, tx.err
}
