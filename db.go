package sqlx

import (
	"database/sql"
	"sync"
)

// DB is a database handle representing a pool of zero or more underlying connections.
// It's safe for concurrent use by multiple goroutines.
type DB struct {
	rdb *sql.DB

	mutex     sync.RWMutex
	isolation IsolationLevel
}

// Open opens a database specified by its database driver name and a driver-specific data source name.
func Open(driverName string, dataSourceName string) (*DB, error) {
	rdb, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		return nil, err
	}
	return &DB{rdb: rdb}, nil
}

// Close closes the database and prevents new queries from starting.
func (db *DB) Close() error {
	return db.rdb.Close()
}

// SetIsolation sets the default isolation level.
func (db *DB) SetIsolation(level IsolationLevel) {
	db.mutex.Lock()
	defer db.mutex.Unlock()

	db.isolation = level
}

// Begin starts a transaction.
func (db *DB) Begin(readonly bool) *Tx {
	db.mutex.RLock()
	defer db.mutex.RUnlock()

	return &Tx{
		Isolation: db.isolation,
		readonly:  readonly,
		rdb:       db.rdb,
	}
}
