# sqlx

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/erdian718/sqlx.svg)](https://pkg.go.dev/gitlab.com/erdian718/sqlx)
[![Go Version](https://img.shields.io/badge/go%20version-%3E=1.20-61CFDD.svg?style=flat-square)](https://go.dev/)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/erdian718/sqlx)](https://goreportcard.com/report/gitlab.com/erdian718/sqlx)

Package sqlx provides extra features for go's standard database/sql library.

## Feature

* The APIs is very concise.
* Support "range-over-func" feature.
* Conveniently map query results.
* Conveniently pass named query parameters.

## Usage

```go
import "gitlab.com/erdian718/sqlx"

func main() {
	db, err := sqlx.Open(...)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	tx := db.Begin(true)
	defer tx.Rollback()

	err = tx.Stmt("...").Query(...)
	if err != nil {
		panic()
	}
}
```

## Note

* Currently, only struct and struct pointer mapping are supported.
